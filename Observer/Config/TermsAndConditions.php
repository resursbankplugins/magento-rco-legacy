<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Rco\Observer\Config;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Value;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Store\Model\ScopeInterface;
use Resursbank\Rco\Helper\Config;

/**
 * This observer will print an error message, notifying clients that the RCO
 * API flow, and the "Enable Terms and Conditions" cannot be enabled at the
 * same time.
 */
class TermsAndConditions implements ObserverInterface
{
    /**
     * @var ManagerInterface
     */
    private ManagerInterface $message;

    /**
     * @var ScopeConfigInterface
     */
    private ScopeConfigInterface $scopeConfig;

    /**
     * @var Config
     */
    private Config $config;

    /**
     * @param ManagerInterface $message
     * @param ScopeConfigInterface $scopeConfig
     * @param Config $config
     */
    public function __construct(
        ManagerInterface $message,
        ScopeConfigInterface $scopeConfig,
        Config $config
    ) {
        $this->message = $message;
        $this->scopeConfig = $scopeConfig;
        $this->config = $config;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer): void
    {
        $dataObject = $observer->getEvent()->getData('data_object');

        if ($dataObject instanceof Value) {
            $data = $dataObject->getData();
            $termsEvData = $this->getTermsAndConditionsFromEventData($data);
            $termsConfig = $this->isTermsAndConditionsFromConfig();
            $apiFlowEvData = $this->getApiFlowFromEventData($data);

            if (($apiFlowEvData === Config::API_FLOW_OPTION && $termsConfig) ||
                ($termsEvData && $this->isApiFlowFromConfig())
            ) {
                $this->message->addErrorMessage(__(
                    'You must disable your terms and conditions under ' .
                    'Sales -> Checkout in order for the RCO checkout ' .
                    'to function properly.'
                ));
            }
        }
    }

    /**
     * @return bool
     */
    private function isApiFlowFromConfig(): bool
    {
        return $this->config->isActive(null);
    }

    /**
     * @return bool
     */
    private function isTermsAndConditionsFromConfig(): bool
    {
        return $this->scopeConfig->isSetFlag(
            'checkout/options/enable_agreements',
            ScopeInterface::SCOPE_STORES
        );
    }

    /**
     * @param array $data
     * @return bool|null
     */
    private function getTermsAndConditionsFromEventData(
        array $data
    ): ?bool {
        return $data['path'] === 'checkout/options/enable_agreements' ?
            (bool) $data['value'] :
            null;
    }

    /**
     * @param array $data
     * @return string|null
     */
    private function getApiFlowFromEventData(
        array $data
    ): ?string {
        return $data['path'] === 'resursbank/api/flow' ?
            $data['value'] :
            null;
    }
}
