# Resurs Bank - Magento 2 module - Checkout Flow (RCO)

## Description

Checkout Flow (RCO) API implementation for Magento 2 module.

---

## Prerequisites

* [Magento 2](https://devdocs.magento.com/guides/v2.4/install-gde/bk-install-guide.html) [Supports Magento 2.4.1+]

---

## Changelog

#### 1.2.0

* Temporarily removed feature to lock iframe while totals are updating (to avoid gui glitches).
* Added option to enable info message when shipping methods re-render.

#### 1.2.1

* Updated metadata in composer.json
* Removed unrelated test file.

#### 1.5.2

* Replaced logotypes.
* Added PHP 8.1 support.

#### 1.5.3

* iFrame is no longer rendered without API credentials.

#### 1.5.4

* Removed JS code that broke checkout process in 2.4.5
