/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

define(
    [],
    /**
     * @returns {Readonly<Rco.Model.Session>}
     */
    function () {
        'use strict';

        /**
         * @constant
         * @type {object}
         */
        var PRIVATE = {
            /**
             * The payment session URL.
             *
             * @type {string}
             */
            url: '',

            /**
             * The payment session ID.
             *
             * @type {string}
             */
            id: '',

            /**
             * API account.
             *
             * @type {string}
             */
            activeUsername: '',

            /**
             * API environment.
             *
             * @type {string}
             */
            environment: '',

            /**
             * Show alert box with info when shipping method block updates.
             *
             * @type {boolean}
             */
            isAlertShippingMethodUpdates: false,

            /**
             * Whether or not the model has been initialized.
             *
             * @type {boolean}
             */
            initialized: false
        };

        /**
         * Public properties.
         *
         * @namespace Rco.Model.Session
         * @constant
         */
        var EXPORT = {
            /**
             * Initializer.
             *
             * @param {string} activeUsername
             * @param {string} environment
             * @param {string} url - Session URL.
             * @param {string} id - ID of the payment session.
             * @return {boolean} Whether the model has been initialized.
             */
            init: function (
                activeUsername,
                environment,
                url,
                id,
                isAlertShippingMethodUpdates
            ) {
                if (!PRIVATE.initialized) {
                    if (typeof url === 'string') {
                        PRIVATE.url = url;
                    }

                    if (typeof id === 'string') {
                        PRIVATE.id = id;
                    }

                    if (typeof activeUsername === 'string') {
                        PRIVATE.activeUsername = activeUsername;
                    }

                    if (typeof environment === 'string') {
                        PRIVATE.environment = environment;
                    }

                    if (typeof isAlertShippingMethodUpdates === 'boolean') {
                        PRIVATE.isAlertShippingMethodUpdates = isAlertShippingMethodUpdates;
                    }

                    PRIVATE.initialized = true;
                }

                return PRIVATE.initialized;
            },

            /**
             * Returns the payment session ID.
             *
             * @returns {string}
             */
            getId: function () {
                return PRIVATE.id;
            },

            /**
             * Returns the payment session URL.
             *
             * @returns {string}
             */
            getUrl: function () {
                return PRIVATE.url;
            },

            /**
             * Returns the account name.
             *
             * @returns {string}
             */
            getActiveUsername: function () {
                return PRIVATE.activeUsername;
            },

            /**
             * Returns the API environment.
             *
             * @returns {string}
             */
            getEnvironment: function () {
                return PRIVATE.environment;
            },

            /**
             * Whether to display alert when shipping method block updates.
             *
             * @returns {boolean}
             */
            isAlertShippingMethodUpdates: function () {
                return PRIVATE.isAlertShippingMethodUpdates;
            },

            /**
             * Returns the state of initialization.
             *
             * @returns {boolean}
             */
            isInitialized: function () {
                return PRIVATE.initialized;
            }
        };

        return Object.freeze(EXPORT);
    }
);
