/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

define(
    [
        'ko',
        'Resursbank_Rco/js/lib/payment',
        'Resursbank_Rco/js/lib/customer'
    ],
    /**
     * @param ko
     * @param {Rco.Lib.Payment} Payment
     * @param {Rco.Lib.Customer} Customer
     * @returns {Readonly<Rco.Lib.Payment>}
     */
    function (
        ko,
        Payment,
        Customer
    ) {
        'use strict';

        /**
         * @callback Rco.Model.Checkout.paymentMethod
         * @param {Rco.Lib.Payment.Method|null} [value]
         * @return {Rco.Lib.Payment.Method|null}
         */

        /**
         * @callback Rco.Model.Checkout.address
         * @param {Rco.Lib.Customer.AddressData|null} [value]
         * @return {Rco.Lib.Customer.AddressData|null}
         */

        /**
         * @constant
         * @type {object}
         */
        var PRIVATE = {
            /**
             * @type {Rco.Model.Checkout.paymentMethod}
             */
            paymentMethod: ko.observable(null),

            /**
             * @type {Rco.Lib.Checkout.address}
             */
            address: ko.observable(null)
        };

        /**
         * @namespace Rco.Model.Checkout
         * @constant
         */
        var EXPORT = {
            /**
             * The selected payment method of the iframe.
             *
             * @type {Rco.Model.Checkout.paymentMethod}
             */
            paymentMethod: ko.computed({
                read: function () {
                    return PRIVATE.paymentMethod();
                },

                write: function (value) {
                    if (Payment.isMethod(value)) {
                        PRIVATE.paymentMethod(value);
                    }
                }
            }),

            /**
             * The applied address from the iframe.
             *
             * @type {Rco.Model.Checkout.address}
             */
            address: ko.computed({
                read: function () {
                    return PRIVATE.address();
                },

                write: function (value) {
                    if (Customer.isValid(value)) {
                        PRIVATE.address(value);
                    }
                }
            })
        };

        return Object.freeze(EXPORT);
    }
);
