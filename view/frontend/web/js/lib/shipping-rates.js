/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

define(
    [
        'jquery',
        'Magento_Ui/js/modal/alert'
    ],
    /**
     * @param $
     * @param UiAlert
     * @returns {Readonly<Rco.Lib.ShippingRates>}
     */
    function (
        $,
        UiAlert
    ) {
        /**
         * @typedef {object} Rco.Lib.ShippingRates.Rate
         * @property {string} carrier_code
         * @property {string} method_code
         * @property {string|null} carrier_title
         * @property {string|null} method_title
         * @property {number} amount
         * @property {number} base_amount
         * @property {boolean} available
         * @property {string} error_message
         * @property {number} price_excl_tax
         * @property {number} price_incl_tax
         * @see vendor/magento/module-quote/Api/Data/ShippingMethodInterface
         */

        /**
         * @typedef {object} Rco.Lib.ShippingRates.Changes
         * @property {boolean} hasChanged
         * @property {Rco.Lib.ShippingRates.Rate[]} new
         * @property {Rco.Lib.ShippingRates.Rate[]} removed
         * @property {Rco.Lib.ShippingRates.Rate[]} updated
         */

        /**
         * @constant
         * @namespace Rco.Lib.ShippingRates
         */
        var EXPORT = {
            /**
             * Creates and displays an alert message to the customer that will
             * inform them that the list of available shipping rates has been
             * updated, and that they should take another look at it before
             * placing their order.
             *
             * @returns {this}
             */
            createUpdatedAlert: function () {
                var infoParagraph1 = $(document.createElement('p'))
                    .text($.mage.__(
                        'The list of available shipping methods has ' +
                        'updated. New options may have been added or prices ' +
                        'of existing ones can have changed.'
                    ));

                var infoParagraph2 = $(document.createElement('p'))
                    .text($.mage.__(
                        'Please return to the step where you choose a ' +
                        'shipping method and confirm your selection.'
                    ));

                var logo = $(document.createElement('div'))
                    .addClass('resursbank-rco-alert-logo');

                var info = $(document.createElement('div'))
                    .addClass('resursbank-rco-alert-info')
                    .append(infoParagraph1)
                    .append(infoParagraph2);

                var content = $(document.createElement('div'))
                    .addClass('resursbank-rco-alert-content')
                    .append(logo)
                    .append(info);

                UiAlert({
                    title: '',
                    content: content,
                    clickableOverlay: false,
                    focus: 'none',
                    autoOpen: false,
                    modalClass: 'resursbank-rco-alert',
                    buttons: [{
                        text: $.mage.__('OK'),
                        class: 'action primary accept',

                        click: function () {
                            this.closeModal(true);
                        }
                    }]
                });

                return EXPORT;
            },

            /**
             * Checks if any changes has been made between an old set of
             * shipping methods and a new set. Changes include:
             *
             * * If there are new methods.
             * * If any of the old methods have been removed.
             * * If prices of any of the old methods have changed.
             *
             * @param {Rco.Lib.ShippingRates.Rate[]} oldRates
             * @param {Rco.Lib.ShippingRates.Rate[]} newRates
             * @returns {boolean}
             */
            hasChanged: function (
                oldRates,
                newRates
            ) {
                return oldRates.length !== newRates.length ||
                    oldRates.some(function (rate) {
                        return EXPORT.isUpdated(rate, newRates) ||
                            EXPORT.isRemoved(rate, newRates);
                    });
            },

            /**
             * Check a shipping method against a list if it still exists but
             * its price has been updated.
             *
             * @param {Rco.Lib.ShippingRates.Rate} rate
             * @param {Rco.Lib.ShippingRates.Rate[]} newRates
             * @returns {boolean}
             */
            isUpdated: function (
                rate,
                newRates
            ) {
                return newRates.some(function (newRate) {
                    return rate.carrier_code === newRate.carrier_code &&
                        rate.price_incl_tax !== newRate.price_incl_tax;
                });
            },

            /**
             * Check if a shipping method has been removed from a list of
             * shipping methods.
             *
             * @param {Rco.Lib.ShippingRates.Rate} rate
             * @param {Rco.Lib.ShippingRates.Rate[]} newRates
             * @returns {boolean}
             */
            isRemoved: function (
                rate,
                newRates
            ) {
                return newRates.every(function (newRate) {
                    return rate.carrier_code !== newRate.carrier_code;
                });
            }
        };

        return Object.freeze(EXPORT);
    }
);
