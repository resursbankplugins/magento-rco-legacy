/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

define([], function () {
    'use strict';

    var mixin = {
        /**
         * This method is originally used by summary ui-components to decide
         * whether the prices in the order summary (discount, shipping, etc.)
         * will be displayed.
         *
         * In it's original method, Magento checks if the shipping step has been
         * processed, at which point it will return "true", and the prices will
         * be displayed. In this module the shipping step is disabled, so we
         * will never get to the billing step, so it will always return "false"
         * and the prices will never be displayed.
         *
         * @returns {boolean}
         */
        isFullMode: function () {
            return Boolean(this.getTotals());
        }
    }

    return function (target) {
        return target.extend(mixin);
    };
});
