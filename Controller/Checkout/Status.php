<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Rco\Controller\Checkout;

use Exception;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Controller\Result\Json;
use Resursbank\Core\Helper\Log;
use Resursbank\Rco\Helper\Session;

class Status implements HttpGetActionInterface
{
    /**
     * @var Context
     */
    private Context $context;

    /**
     * @var Log
     */
    private Log $log;

    /**
     * @var Session
     */
    private Session $session;

    /**
     * @var CheckoutSession
     */
    private CheckoutSession $checkoutSession;

    /**
     * @param Context $context
     * @param CheckoutSession $checkoutSession
     * @param Log $log
     * @param Session $session
     */
    public function __construct(
        Context $context,
        CheckoutSession $checkoutSession,
        Log $log,
        Session $session
    ) {
        $this->context = $context;
        $this->log = $log;
        $this->session = $session;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * Checks the status of the payment session and if the grand total is zero.
     *
     * @return ResultInterface
     * @noinspection InvertedIfElseConstructsInspection
     */
    public function execute(): ResultInterface
    {
        /** @var Json $response */
        $response = $this->context->getResultFactory()->create(
            ResultFactory::TYPE_JSON
        );

        /** @noinspection BadExceptionsProcessingInspection */
        try {
            $data = [
                'isSessionValid' => false,
                'isGrandTotalZero' => false,
                'invalidSessionError' => '',
            ];

            if (!$this->session->isInitialized()) {
                $data['invalidSessionError'] = __(
                    'We apologize, your session has expired and the page ' .
                    'will reload after this message has been dismissed. ' .
                    'Your order has not yet been placed. Please fill out ' .
                    'your information below after reloading to proceed with ' .
                    'placing your order.'
                );
            } else {
                $data['isSessionValid'] = true;
                $data['isGrandTotalZero'] =
                    (float) $this->checkoutSession
                    ->getQuote()
                    ->getGrandTotal() <= 0.0;
            }

            $response->setData($data);
        } catch (Exception $e) {
            $this->log->exception($e);
        }

        return $response;
    }
}
