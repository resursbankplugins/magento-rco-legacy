<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Rco\Plugin\Layout\File;

use Exception;
use Magento\Framework\Exception\ValidatorException;
use Magento\Framework\View\Layout\File\Collector\Aggregated;
use Magento\Framework\View\File;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Resursbank\Rco\Helper\Config;
use Resursbank\Core\Helper\Api\Credentials;
use Resursbank\Rco\Helper\Log;
use function strpos;

/**
 * This plugin will exclude our overriding layout file for checkout_index_index
 * if the module has been disabled.
 *
 * This plugin is necessary to ensure different API flows may be applied
 * individually in each store. Otherwise, the extending checkout_index_index.xml
 * files might conflict with each other.
 */
class Collector
{
    /**
     * @var Config
     */
    private Config $config;

    /**
     * @var Credentials
     */
    private Credentials $credentials;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @var Log
     */
    private Log $log;

    /**
     * @param Config $config
     * @param Credentials $credentials
     * @param StoreManagerInterface $storeManager
     * @param Log $log
     */
    public function __construct(
        Config $config,
        Credentials $credentials,
        StoreManagerInterface $storeManager,
        Log $log
    ) {
        $this->config = $config;
        $this->credentials = $credentials;
        $this->storeManager = $storeManager;
        $this->log = $log;
    }

    /**
     * If this module is disabled, remove our checkout_index_index.xml file
     * from the collection of XML files assembled by Magento.
     *
     * @param Aggregated $subject
     * @param array<mixed> $result
     * @return File[]
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @noinspection PhpUnusedParameterInspection
     */
    public function afterGetFiles(
        Aggregated $subject,
        array $result
    ): array {
        try {
            $storeCode = $this->storeManager->getStore()->getCode();

            if (!$this->isEnabled($storeCode)) {
                foreach ($result as $key => $file) {
                    if ($this->isOurLayoutFile($file)) {
                        unset($result[$key]);
                    }
                }
            }
        } catch (Exception $e) {
            $this->log->exception($e);
        }

        return $result;
    }

    /**
     * Check if provided layout file is the extending checkout_index_index.xml
     * file that belongs to this module.
     *
     * @param File $file
     * @return bool
     */
    protected function isOurLayoutFile(
        File $file
    ): bool {
        return (
            $file->getModule() === 'Resursbank_Rco' &&
            strpos($file->getFileIdentifier(), 'checkout_index_index.xml')
        );
    }

    /**
     * @param string|null $storeCode
     * @return bool
     * @throws ValidatorException
     */
    private function isEnabled(
        ?string $storeCode
    ): bool {
        $credentials = $this->credentials->resolveFromConfig(
            $storeCode,
            ScopeInterface::SCOPE_STORES
        );

        return (
            $this->config->isActive($storeCode) &&
            $this->credentials->hasCredentials($credentials)
        );
    }
}
