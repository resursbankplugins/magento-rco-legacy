<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Rco\Plugin\PaymentSession;

use Exception;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Resursbank\Core\Helper\Api\Credentials;
use Resursbank\Rco\Helper\Config;
use Resursbank\Rco\Helper\Log;
use Resursbank\Rco\Helper\Session;

class Init
{
    /**
     * @var Credentials
     */
    private Credentials $credentials;

    /**
     * @var Log
     */
    private Log $log;

    /**
     * @var Session
     */
    private Session $session;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @var Config
     */
    private Config $config;

    /**
     * @param Credentials $credentials
     * @param Log $log
     * @param Session $session
     * @param Config $config
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Credentials $credentials,
        Log $log,
        Session $session,
        Config $config,
        StoreManagerInterface $storeManager
    ) {
        $this->credentials = $credentials;
        $this->log = $log;
        $this->session = $session;
        $this->storeManager = $storeManager;
        $this->config = $config;
    }

    /**
     * Initialize payment session before the checkout page loads (pre-dispatch
     * of checkout_index_index).
     *
     * @return void
     * @throws Exception
     */
    public function beforeExecute(): void
    {
        try {
            if ($this->isEnabled()) {
                $this->session->init(
                    $this->credentials->resolveFromConfig(
                        $this->storeManager->getStore()->getCode(),
                        ScopeInterface::SCOPE_STORES
                    )
                );
            }
        } catch (Exception $e) {
            $this->log->exception($e);
        }
    }

    /**
     * Whether this plugin should execute.
     *
     * @return bool
     * @throws NoSuchEntityException
     */
    private function isEnabled(): bool
    {
        $storeCode = $this->storeManager->getStore()->getCode();

        return (
            $this->config->isActive($storeCode) &&
            !$this->session->isInitialized()
        );
    }
}
