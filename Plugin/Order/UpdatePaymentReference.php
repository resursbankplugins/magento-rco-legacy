<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Rco\Plugin\Order;

use Exception;
use Magento\Sales\Model\Order;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Resursbank\Core\Helper\Api\Credentials;
use Resursbank\Rco\Helper\Config;
use Resursbank\Rco\Helper\Log;
use Resursbank\Rco\Helper\Session;

/**
 * This plugin will update the payment reference at Resurs Bank upon order
 * creation, to match the order increment_id in Magento. This is to ensure
 * the payment and order will share the same reference (name) to enable easier
 * identification on both ends.
 */
class UpdatePaymentReference
{
    /**
     * @var Credentials
     */
    private Credentials $credentials;

    /**
     * @var Log
     */
    private Log $log;

    /**
     * @var Session
     */
    private Session $session;

    /**
     * @var Config
     */
    private Config $config;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @param Credentials $credentials
     * @param Log $log
     * @param Session $session
     * @param Config $config
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Credentials $credentials,
        Log $log,
        Session $session,
        Config $config,
        StoreManagerInterface $storeManager
    ) {
        $this->credentials = $credentials;
        $this->log = $log;
        $this->session = $session;
        $this->config = $config;
        $this->storeManager = $storeManager;
    }

    /**
     * Update the Resurs Bank payment reference to correspond with the newly
     * created order increment_id.
     *
     * @param Order $subject
     * @param Order $result
     * @return Order
     * @throws Exception
     */
    public function afterBeforeSave(
        Order $subject,
        Order $result
    ): Order {
        try {
            if ($this->isEnabled($subject)) {
                $this->session->updatePaymentReference(
                    $this->credentials->resolveFromConfig(
                        (string) $subject->getStoreId(),
                        ScopeInterface::SCOPE_STORES
                    ),
                    $this->session->getPaymentReference(),
                    $subject->getIncrementId()
                );
            }
        } catch (Exception $e) {
            $this->log->exception($e);
        }

        return $result;
    }

    /**
     * Check whether this plugin should execute.
     *
     * @param Order $order
     * @return bool
     * @throws Exception
     */
    private function isEnabled(
        Order $order
    ): bool {
        $storeCode = $this->storeManager->getStore()->getCode();

        return (
            $this->config->isActive($storeCode) &&
            $order->isObjectNew() &&
            !$order->getOriginalIncrementId() &&
            $order->getGrandTotal() > 0
        );
    }
}
