<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Rco\Helper;

use Exception;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use function is_string;
use Magento\Customer\Model\Address;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\UrlInterface;
use Resursbank\Core\Exception\ApiDataException;
use Resursbank\Core\Exception\SessionDataException;
use Resursbank\Core\Helper\Api;
use Resursbank\Core\Helper\Url;
use Resursbank\Core\Model\Api\Credentials;
use Resursbank\Core\Model\Api\Payment\Converter\QuoteConverter;
use Resursbank\Core\ViewModel\Session\Checkout as CheckoutSession;
use stdClass;
use function strlen;

/**
 * This class implements ArgumentInterface (that's normally reserved for
 * ViewModels) because we found no other way of removing the suppressed warning
 * for PHPMD.CookieAndSessionMisuse. The interface fools the analytic tools into
 * thinking this class is part of the presentation layer, and thus eligible to
 * handle the session.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Session implements ArgumentInterface
{
    /**
     * Session key for payment reference.
     *
     * @var string
     */
    public const PAYMENT_REFERENCE_KEY = 'resursbank_rco_reference';

    /**
     * Session key for payment session id.
     *
     * @var string
     */
    public const PAYMENT_SESSION_ID_KEY = 'resursbank_rco_id';

    /**
     * Session key for script tag to inject the RCO iframe library.
     *
     * @var string
     */
    public const IFRAME_LIB_SCRIPT_TAG_KEY = 'resursbank_rco_script_tag';

    /**
     * Session key for iframe URL.
     *
     * @var string
     */
    public const IFRAME_URL_KEY = 'resursbank_rco_iframe_url';

    /**
     * @var Api
     */
    private Api $api;

    /**
     * @var CustomerSession
     */
    private CustomerSession $customerSession;

    /**
     * @var CheckoutSession
     */
    private CheckoutSession $checkoutSession;

    /**
     * @var UrlInterface
     */
    private UrlInterface $url;

    /**
     * @var QuoteConverter
     */
    private QuoteConverter $quoteConverter;

    /**
     * @var Url
     */
    private Url $urlHelper;

    /**
     * @param Api $api
     * @param CheckoutSession $checkoutSession
     * @param CustomerSession $customerSession
     * @param QuoteConverter $quoteConverter
     * @param UrlInterface $url
     * @param Url $urlHelper
     */
    public function __construct(
        Api $api,
        CheckoutSession $checkoutSession,
        CustomerSession $customerSession,
        QuoteConverter $quoteConverter,
        UrlInterface $url,
        Url $urlHelper
    ) {
        $this->api = $api;
        $this->checkoutSession = $checkoutSession;
        $this->customerSession = $customerSession;
        $this->quoteConverter = $quoteConverter;
        $this->url = $url;
        $this->urlHelper = $urlHelper;
    }

    /**
     * Initialize a new payment session.
     *
     * @param Credentials $credentials
     * @return void
     * @throws Exception
     */
    public function init(
        Credentials $credentials
    ): void {
        $response = $this->createPayment($credentials);

        // Validate response from creating payment session.
        $this->validateCreatePaymentResponse($response);

        // Append payment session data to client session.
        $this->setIframeLibScriptTag($response->script)
            ->setPaymentSessionId($response->paymentSessionId)
            ->setIframeUrl($response->baseUrl);
    }

    /**
     * Update existing payment.
     *
     * @param Credentials $credentials
     * @throws Exception
     */
    public function update(
        Credentials $credentials
    ): void {
        $this->api
            ->getConnection($credentials)
            ->updateCheckoutOrderLines(
                $this->getPaymentReference(),
                ['orderLines' => $this->getOrderLines()]
            );
    }

    /**
     * Update the payment reference.
     *
     * @param Credentials $credentials
     * @param string $paymentId
     * @param string $newReference
     * @return void
     * @throws Exception
     */
    public function updatePaymentReference(
        Credentials $credentials,
        string $paymentId,
        string $newReference
    ): void {
        $this->api
            ->getConnection($credentials)
            ->updatePaymentReference($paymentId, $newReference);

        $this->setPaymentReference($newReference);
    }

    /**
     * Retrieve payment reference from session.
     *
     * @return string
     * @throws SessionDataException
     */
    public function getPaymentReference(): string
    {
        $result = $this->checkoutSession->getData(self::PAYMENT_REFERENCE_KEY);

        if (!is_string($result)) {
            throw new SessionDataException(
                __('Payment reference is not a string.')
            );
        }

        return $result;
    }

    /**
     * Set payment reference in session.
     *
     * @param string $reference
     * @return self
     */
    public function setPaymentReference(
        string $reference
    ): self {
        $this->checkoutSession->setData(
            self::PAYMENT_REFERENCE_KEY,
            $reference
        );

        return $this;
    }

    /**
     * Retrieve RCO iframe library script HTML-tag from session.
     *
     * @return string
     * @throws SessionDataException
     */
    public function getIframeLibScriptTag(): string
    {
        $result = $this->checkoutSession->getData(
            self::IFRAME_LIB_SCRIPT_TAG_KEY
        );

        if (!is_string($result)) {
            throw new SessionDataException(__('Script tag is not a string.'));
        }

        return $result;
    }

    /**
     * Store RCO iframe library script HTM-tag in session.
     *
     * @param string $html
     * @return self
     */
    public function setIframeLibScriptTag(
        string $html
    ): self {
        $this->checkoutSession->setData(self::IFRAME_LIB_SCRIPT_TAG_KEY, $html);

        return $this;
    }

    /**
     * Retrieve payment session id from session.
     *
     * @return string
     * @throws SessionDataException
     */
    public function getPaymentSessionId(): string
    {
        $result = $this->checkoutSession->getData(self::PAYMENT_SESSION_ID_KEY);

        if (!is_string($result)) {
            throw new SessionDataException(
                __('Payment session id must be a string.')
            );
        }

        return $result;
    }

    /**
     * Store payment session id in session.
     *
     * @param string $value
     * @return self
     */
    public function setPaymentSessionId(
        string $value
    ): self {
        $this->checkoutSession->setData(self::PAYMENT_SESSION_ID_KEY, $value);

        return $this;
    }

    /**
     * Retrieve iframe URL from session.
     *
     * @return string
     * @throws SessionDataException
     */
    public function getIframeUrl(): string
    {
        $result = $this->checkoutSession->getData(self::IFRAME_URL_KEY);

        if (!is_string($result)) {
            throw new SessionDataException(__('Iframe URL must be a string.'));
        }

        return $result;
    }

    /**
     * Set iframe URL in session.
     *
     * @param string $iframeUrl
     * @return self
     */
    public function setIframeUrl(
        string $iframeUrl
    ): self {
        $this->checkoutSession->setData(self::IFRAME_URL_KEY, $iframeUrl);

        return $this;
    }

    /**
     * Get the order lines from the quote to be used in Api call.
     *
     * @return array<array>
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws Exception
     */
    public function getOrderLines(): array
    {
        return $this->quoteConverter->convertItemsToArrays(
            $this->quoteConverter->convert(
                $this->checkoutSession->getQuote()
            )
        );
    }

    /**
     * Retrieve shop url (used for iframe communication, the return value will
     * be the target origin). Basically this is your Magento websites
     * protocol:domain without any trailing slashes. For example
     * http://www.testing.com
     *
     * @return string
     */
    public function getShopUrl(): string
    {
        return rtrim($this->url->getBaseUrl(), '/');
    }

    /**
     * Retrieve customer information used when initializing a payment session.
     *
     * @return array<string, string|null|array>
     */
    public function getCustomerInformation(): array
    {
        /** @var Address|false $billingAddress */
        $billingAddress = $this->getCustomer()->getPrimaryBillingAddress();

        /** @var Address|false $shippingAddress */
        $shippingAddress = $this->getCustomer()->getPrimaryShippingAddress();

        $phone = ($billingAddress instanceof Address) ?
            $billingAddress->getTelephone()
            : null;

        $deliveryAddress = ($shippingAddress instanceof Address) ?
            [
                'firstName' => $shippingAddress->getFirstname(),
                'lastName' => $shippingAddress->getLastname(),
                'addressRow1' => $shippingAddress->getStreetLine(1),
                'addressRow2' => $shippingAddress->getStreetLine(2),
                'postalArea' => $shippingAddress->getCity(),
                'postalCode' => $shippingAddress->getPostcode(),
                'countryCode' => $shippingAddress->getCountry()
            ] : null;

        return [
            'mobile' => $phone,
            'email' => $this->getCustomer()->getData('email'),
            'deliveryAddress' => $deliveryAddress
        ];
    }

    /**
     * Get the currently logged in customer.
     *
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customerSession->getCustomer();
    }

    /**
     * Retrieve the payment payload.
     *
     * @return array<string, mixed>
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws Exception
     */
    private function getPaymentPayload(): array
    {
        $quoteId = (int) $this->checkoutSession->getQuoteId();

        return [
            'orderLines' => $this->getOrderLines(),
            'successUrl' => $this->urlHelper->getSuccessUrl($quoteId),
            'backUrl' => $this->urlHelper->getFailureUrl($quoteId),
            'shopUrl' => $this->getShopUrl(),
            'customer' => $this->getCustomerInformation()
        ];
    }

    /**
     * Generate a new payment reference and put it into session.
     *
     * @return self
     * @throws Exception
     */
    private function createNewPaymentReference(): self
    {
        $this->setPaymentReference(
            $this->generateNewPaymentReference()
        );

        return $this;
    }

    /**
     * Generate a random payment reference string.
     *
     * @param int $length
     * @return string
     * @throws Exception
     * @noinspection PhpSameParameterValueInspection
     */
    private function generateNewPaymentReference(
        int $length = 16
    ): string {
        $result = '';

        /** @noinspection PhpAssignmentInConditionInspection */
        while (($len = strlen($result)) < $length) {
            $size = $length - $len;
            $bytes = random_bytes($size);
            $result .= substr(
                str_replace(
                    ['/', '+', '='],
                    '',
                    base64_encode($bytes)
                ),
                0,
                $size
            );
        }

        return $result;
    }

    /**
     * Check if payment session has been initialized.
     *
     * @return bool
     * @noinspection BadExceptionsProcessingInspection
     */
    public function isInitialized(): bool
    {
        try {
            $result = (
                $this->getPaymentReference() !== '' &&
                $this->getPaymentSessionId() !== '' &&
                $this->getIframeLibScriptTag() !== '' &&
                $this->getIframeUrl() !== ''
            );
        } catch (SessionDataException $e) {
            $result = false;
        }

        return $result;
    }

    /**
     * Clear session data.
     */
    public function clear(): void
    {
        $this->checkoutSession->unsetData([
            self::IFRAME_URL_KEY,
            self::IFRAME_LIB_SCRIPT_TAG_KEY,
            self::PAYMENT_SESSION_ID_KEY,
            self::PAYMENT_REFERENCE_KEY
        ]);
    }

    /**
     * @param Credentials $credentials
     * @return stdClass
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws Exception
     */
    private function createPayment(
        Credentials $credentials
    ): stdClass {
        $connection = $this->api->getConnection($credentials);

        // Create payment.
        $connection->createPayment(
            $this->createNewPaymentReference()->getPaymentReference(),
            $this->getPaymentPayload()
        );

        /** @var stdClass|null $response */
        $response = $connection->getFullCheckoutResponse();

        if (!$response instanceof stdClass) {
            throw new ApiDataException(
                __(
                    'Payment session initialization failed. ' .
                    'Unexpected datatype, expected instance of stdClass.'
                )
            );
        }

        return $response;
    }

    /**
     * Validate API response from initiating new payment session.
     *
     * @param stdClass $response
     * @throws ApiDataException
     */
    private function validateCreatePaymentResponse(
        stdClass $response
    ): void {
        $error = '';

        // Validate paymentSessionId property on response object.
        if (!isset($response->paymentSessionId)) {
            $error = 'Missing paymentSessionId property in response.';
        }

        if (!is_string($response->paymentSessionId)) {
            $error = 'paymentSessionId property must be a string.';
        }

        // Validate iframe property on response object.
        if (!isset($response->iframe)) {
            $error = 'Missing iframe property in response.';
        }

        if (!is_string($response->iframe)) {
            $error = 'iframe property must be a string.';
        }

        // Validate script property on response object.
        if (!isset($response->script)) {
            $error = 'Missing script property in response.';
        }

        if (!is_string($response->script)) {
            $error = 'script property must be a string.';
        }

        if ($error !== '') {
            throw new ApiDataException(
                __('Payment session initialization failed. ' . $error)
            );
        }
    }
}
