<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Rco\Test\Unit\Plugin\Config;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Resursbank\Core\Model\Config\Source\Flow;
use Resursbank\Rco\Helper\Config;
use Resursbank\Rco\Plugin\Config\AddFlowOption;

class AddFlowOptionTest extends TestCase
{
    /**
     * @var AddFlowOption
     */
    private AddFlowOption $addFlowOption;

    /**
     * @inheriDoc
     */
    public function setUp(): void
    {
        $this->addFlowOption = new AddFlowOption();
    }

    /**
     * Assert that afterToArray returns array including Config::API_FLOW_OPTION.
     */
    public function testAfterToArray(): void
    {
        $testArray = [
            'key' => __('value')
        ];
        $expectedArray = [
            'key' => __('value'),
            Config::API_FLOW_OPTION => 'Onepage iFrame based Resurs Checkout'
        ];
        $flowMock = $this->createMock(Flow::class);

        self::assertEquals($expectedArray, $this->addFlowOption->afterToArray($flowMock, $testArray));
    }
}
