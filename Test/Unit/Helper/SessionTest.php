<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Rco\Test\Unit\Helper;

use Exception;
use Resursbank\Core\ViewModel\Session\Checkout as CheckoutSession;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\UrlInterface;
use PHPUnit\Framework\TestCase;
use Resursbank\Core\Exception\SessionDataException;
use Resursbank\Core\Helper\Api;
use Resursbank\Core\Helper\Url;
use Resursbank\Core\Model\Api\Payment\Converter\QuoteConverter;
use Resursbank\Rco\Helper\Session;

/**
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class SessionTest extends TestCase
{
    /**
     * @var Session
     */
    private Session $session;

    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @var UrlInterface
     */
    private $url;

    /**
     * @inheritDoc
     */
    protected function setUp(): void
    {
        // Create a mocked instance of Magento Checkout Session, so we can
        // manipulate return values from the client session.
        $this->checkoutSession = $this->getMockBuilder(CheckoutSession::class)
            ->disableOriginalConstructor()
            ->onlyMethods([
                'getData',
            ])->addMethods([
                'getPaymentReference',
                'getSuccessUrl',
                'getOrderLines',
                'getCustomer',
                'getCustomerInformation',
            ])
            ->getMock();

        $this->url = $this->getMockBuilder(UrlInterface::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getUrl'])
            ->getMockForAbstractClass();
        $api = $this->createMock(Api::class);
        $customerSession = $this->createMock(CustomerSession::class);
        $quoteConverter = $this->createMock(QuoteConverter::class);
        $url = $this->createMock(Url::class);

        // Create an instance of the RCO Session model, passing in our mocked
        // instance of Magento's Checkout Session.
        $this->session = new Session(
            $api,
            $this->checkoutSession,
            $customerSession,
            $quoteConverter,
            $this->url,
            $url
        );
    }

    /**
     * Assert that getPaymentReference method will return the string stored
     * within the client session.
     */
    public function testGetPaymentReferenceReturnsString(): void
    {
        // This is the reference we expect to exists within the session.
        $reference = 'asd43rfSDfsdf232dfSDf11wefsd345';

        // The getPaymentReference() method will call the getData(). We assert
        // the getData() method is called exactly once, and we manipulate it's
        // return value.
        /** @phpstan-ignore-next-line */
        $this->checkoutSession
            ->expects(static::once())
            ->method('getData')
            ->willReturn($reference);

        try {
            static::assertSame(
                $reference,
                $this->session->getPaymentReference()
            );
        } catch (Exception $e) {
            // If there is any Exception we automatically fail the test.
            static::fail(
                'Failed to validate return value of getPaymentReference: ' .
                $e->getMessage()
            );
        }
    }

    /**
     * Assert that getPaymentReference method will throw an instance of
     * SessionDataException if the value stored within the session is NULL.
     * @throws SessionDataException
     */
    public function testGetPaymentReferenceThrowsOnNull(): void
    {
        // Since we do not modify the return value of the getData() method call
        // (like we do in testGetPaymentReferenceReturnsString() above), the
        // value returned from the Checkout Session instance will be NULL.
        // This should result in an instance of SessionDataException being
        // thrown, so that's what we assert here.
        $this->expectException(SessionDataException::class);

        $this->session->getPaymentReference();
    }

    /**
     * Assert that getPaymentReference method will throw an instance of
     * SessionDataException if the value stored within the session is not of
     * type string.
     * @throws SessionDataException
     */
    public function testGetPaymentReferenceThrowsWithoutString(): void
    {
        $reference = 123123;

        // Again, assert that the getData() method on the Checkout Session
        // instance is called exactly once, and modify the return value to be
        // $reference.
        /** @phpstan-ignore-next-line */
        $this->checkoutSession
            ->expects(static::once())
            ->method('getData')
            ->willReturn($reference);

        // Since the getPaymentReference() method must return a string the int
        // within the client session will cause an instance of
        // SessionDataException to be thrown, so that's what we assert here.
        $this->expectException(SessionDataException::class);

        $this->session->getPaymentReference();
    }

    /**
     * @throws SessionDataException
     */
    public function testGetPaymentSessionId(): void
    {
        $sessionId = 'sessionIdString';

        /** @phpstan-ignore-next-line */
        $this->checkoutSession
            ->expects(static::once())
            ->method('getData')
            ->willReturn($sessionId);

        static::assertSame($sessionId, $this->session->getPaymentSessionId());
    }

    /**
     * @throws SessionDataException
     */
    public function testGetPaymentSessionIdNull(): void
    {
        $this->expectException(SessionDataException::class);
        $this->session->getPaymentSessionId();
    }

    /**
     * @throws SessionDataException
     */
    public function testGetIframeUrl(): void
    {
        $url = 'https://omnitest.resurs.com/web/dist/omni-checkout.html?session-reference';

        /** @phpstan-ignore-next-line */
        $this->checkoutSession
            ->expects(static::once())
            ->method('getData')
            ->willReturn($url);

        static::assertSame($url, $this->session->getIframeUrl());
    }

    /**
     * @throws SessionDataException
     */
    public function testGetIframeLibScriptTag(): void
    {
        $scriptTag = '<script type="text/javascript"'.
            ' src="https://omnitest.resurs.com/web/dist/js/oc-shop.js"></script>';

        /** @phpstan-ignore-next-line */
        $this->checkoutSession
            ->expects(static::once())
            ->method('getData')
            ->willReturn($scriptTag);

        static::assertSame($scriptTag, $this->session->getIframeUrl());
    }

    /**
     * Test shopUrl, should normally only return the PROTOCOL://HOSTNAME, nothing else.
     */
    public function testGetShopUrl(): void
    {
        $shopUrl = 'http://my-store.test.com';

        /** @phpstan-ignore-next-line */
        $this->url->expects(static::once())
            ->method('getBaseUrl')
            ->willReturn($shopUrl);

        static::assertSame($shopUrl, $this->session->getShopUrl());
    }

    /**
     * Test fetching order lines from session.
     */
    public function testGetOrderLines(): void
    {
        $orderLines = [
            [
                'artNo' => '99art99',
                'description' => 'The ultimate article',
                'quantity' => 1.0,
                'unitMeasure' => 'st',
                'unitAmountWithoutVat' => 45.0,
                'vatPct' => 0,
                'type' => 'ORDER_LINE',
            ],
        ];

        /** @phpstan-ignore-next-line */
        $this->checkoutSession
            ->expects(static::once())
            ->method('getOrderLines')
            ->willReturn($orderLines);

        /** @phpstan-ignore-next-line */
        static::assertSame($orderLines, $this->checkoutSession->getOrderLines());
    }

    /**
     * Test getCustomerInformation (initially array with nulled values, when entering checkout first time).
     */
    public function testGetCustomerInformationInitiallyNull(): void
    {
        $customerInformation = [
            [
                'mobile' => null,
                'email' => null,
            ],
        ];

        /** @phpstan-ignore-next-line */
        $this->checkoutSession
            ->expects(static::once())
            ->method('getCustomerInformation')
            ->willReturn($customerInformation);

        /** @phpstan-ignore-next-line */
        static::assertSame($customerInformation, $this->checkoutSession->getCustomerInformation());
    }

    /**
     * Test getCustomer. Returns its class when running through getCustomerInformation.
     */
    public function testGetCustomer(): void
    {
        /** @phpstan-ignore-next-line */
        $this->checkoutSession
            ->expects(static::once())
            ->method('getCustomer')
            ->willReturn(Customer::class);

        /** @phpstan-ignore-next-line */
        static::assertSame(Customer::class, $this->checkoutSession->getCustomer());
    }
}
