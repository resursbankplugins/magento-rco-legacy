<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Rco\Test\Unit\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Resursbank\Core\Helper\Config as CoreConfig;
use Resursbank\Rco\Helper\Config;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class ConfigTest extends TestCase
{
    /**
     * @var Config
     */
    private Config $config;

    /**
     * @var MockObject|CoreConfig
     */
    private $coreConfigMock;

    protected function setUp(): void
    {
        $contextMock = $this->createMock(Context::class);
        $writerInterfaceMock = $this->createMock(WriterInterface::class);
        $scopeConfigInterfaceMock = $this->createMock(ScopeConfigInterface::class);
        $this->coreConfigMock = $this->createMock(CoreConfig::class);

        $this->config = new Config(
            $scopeConfigInterfaceMock,
            $writerInterfaceMock,
            $this->coreConfigMock,
            $contextMock
        );
    }

    /**
     * Assert that isActive return the correct value.
     */
    public function testIsActiveEnabled(): void
    {
        /** @phpstan-ignore-next-line Undefined method. */
        $this->coreConfigMock->method('getFlow')->with('')->willReturn(Config::API_FLOW_OPTION);

        self::assertTrue($this->config->isActive(''));
    }

    /**
     * Assert that isActive returns true on store specific store if enabled.
     */
    public function testIsActiveEnabledReturnsTrueForSpecificStore(): void
    {
        /** @phpstan-ignore-next-line Undefined method. */
        $this->coreConfigMock
            ->method('getFlow')
            ->with('en', ScopeInterface::SCOPE_STORES)
            ->willReturn(Config::API_FLOW_OPTION);

        self::assertTrue($this->config->isActive('en'));
    }

    /**
     * Assert that isActive returns false if disabled on specific store.
     */
    public function testIsActiveEnabledReturnsFalseForSpecificStore(): void
    {
        /** @phpstan-ignore-next-line Undefined method. */
        $this->coreConfigMock
            ->method('getFlow')
            ->with('se', ScopeInterface::SCOPE_STORES)
            ->willReturn('Something else');

        self::assertFalse($this->config->isActive('se'));
    }

    /**
     * Assert that isActive returns true for specific store if disabled on others.
     */
    public function testIsActiveEnabledReturnsFalseForSpecificStoreIfEnableOnOther(): void
    {
        /** @phpstan-ignore-next-line Undefined method. */
        $this->coreConfigMock->method('getFlow')->withConsecutive(
            ['en', ScopeInterface::SCOPE_STORES],
            ['se', ScopeInterface::SCOPE_STORES],
        )->willReturnOnConsecutiveCalls(Config::API_FLOW_OPTION, 'something else');

        self::assertTrue($this->config->isActive('en'));
        self::assertFalse($this->config->isActive('se'));
    }
}
