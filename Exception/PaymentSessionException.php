<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Rco\Exception;

use Magento\Framework\Exception\LocalizedException;

/**
 * Indicates a problem with the payment session data.
 */
class PaymentSessionException extends LocalizedException
{

}
